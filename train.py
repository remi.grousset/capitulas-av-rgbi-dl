import warnings

from pytorch_lightning.utilities.warnings import LightningDeprecationWarning

warnings.simplefilter(action='ignore', category=LightningDeprecationWarning)

import pytorch_lightning as pl

from ellipse_rcnn import get_dataloaders, EllipseRCNN

if __name__ == "__main__":
    train_loader, validation_loader, test_loader = get_dataloaders(
        ".\\data\\simulated_ellipses_rectified_matrices.h5", batch_size=32,
        num_workers=8)

    model = EllipseRCNN(min_size=512, rpn_batch_size_per_image=512)

    trainer = pl.Trainer(gpus=1, log_every_n_steps=50, reload_dataloaders_every_n_epochs=50,
                         reload_dataloaders_every_epoch=True, default_root_dir=".\\data\\results",
                         max_epochs=300, weights_save_path=".\\data\\results\\saves", auto_lr_find=True,
                         terminate_on_nan=True)

    trainer.fit(model, train_loader)
