<a href="https://pytorch.org/get-started/locally/"><img alt="PyTorch" src="https://img.shields.io/badge/PyTorch-ee4c2c?logo=pytorch&logoColor=white"></a>
<a href="https://pytorchlightning.ai/"><img alt="Lightning" src="https://img.shields.io/badge/-Lightning-792ee5"></a>

# Ellipse R-CNN
A PyTorch (Lightning) implementation of Ellipse R-CNN. Originally developed for another project: [Autonomous Lunar Orbit Navigation Using Ellipse R-CNN and Crater Pattern Matching](https://github.com/wdoppenberg/crater-detection).
