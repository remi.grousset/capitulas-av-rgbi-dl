import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse
import random
import cv2
import h5py
from ellipse_rcnn.utils.conics import conic_matrix


def from_matrix_to_params_2(tensor):
    params = {"height": None,
              "width": None,
              "x": None,
              "y": None,
              "angle": None}

    mat = tensor.detach()
    M0 = mat.numpy()

    # M = mat[1:, 1:].numpy()
    #
    # A = M0[1, 1]
    # B = M0[2, 1] * 2
    # C = M0[2, 2]
    # D = M0[0, 1] * 2
    # E = M0[0, 2]

    # # old
    # a = np.linalg.inv(M0[:2, :2])
    # b = np.expand_dims(-M0[:2, 2], axis=-1)
    # c = a @ b
    #
    # lambdas = np.linalg.eigvalsh(M0[..., :2, :2]) / (-np.linalg.det(M0) / np.linalg.det(M0[..., :2, :2]))[..., None]

    # new
    a = np.linalg.inv(M0[1:, 1:])
    b = np.expand_dims(-M0[0, 1:], axis=1)
    c = a @ b

    lambdas = np.linalg.eigvalsh(M0[..., 1:, 1:]) / (-np.linalg.det(M0) / np.linalg.det(M0[..., 1:, 1:]))[..., None]

    if lambdas[0] <= 0 or lambdas[1] <= 0:
        print("non acceptable values for lambda")
        axes = np.array([2000, 2000])
    else:
        axes = np.sqrt(1 / lambdas)

    # eigenvalues = np.linalg.eig(M)
    # determinant = np.linalg.det(M)
    # params["height"] = np.sqrt(-np.linalg.det(M0) / (np.linalg.det(M) * eigenvalues[0][0]))
    # params["width"] = np.sqrt(-np.linalg.det(M0) / (np.linalg.det(M) * eigenvalues[0][1]))
    # params["x"] = (B * E - 2 * C * D) / (4 * A * C - B ** 2)
    # params["y"] = (B * D - 2 * A * E) / (4 * A * C - B ** 2)
    # params["angle"] = 0.5 * (np.pi / 2 - np.arctan((A - C) / D))

    params["height"] = axes[0]
    params["width"] = axes[1]
    if axes[0] >= axes[1]:
        params["height"] = axes[0]
        params["width"] = axes[1]
    else:
        params["height"] = axes[1]
        params["width"] = axes[0]
        print("inversion d'axes")

    params["x"] = c[0][0]
    params["y"] = c[1][0]

    # # old
    # params["angle"] = np.arctan2(2 * M0[..., 1, 0], (M0[..., 0, 0] - M0[..., 1, 1])) / 2
    # new
    z = (M0[1, 1] - M0[2, 2]) / (2 * M0[1, 2])
    params["angle"] = np.arctan(1 / z) / 2

    return params


def convert_napari_box_to_ellipsercnn_anns(a, b, h, k, tau):
    c = np.cos(tau)
    s = np.sin(tau)

    A = (b ** 2) * c ** 2 + (a ** 2) * s ** 2
    B = 2 * ((b ** 2) - (a ** 2)) * c * s
    C = (a ** 2) * c ** 2 + b ** 2 * s ** 2
    D = - 2 * A * h - k * B
    F = - 2 * C * k - h * B
    G = - (a * b) ** 2 + A * (h ** 2) + B * h * k + C * (k ** 2)

    out = np.empty((3, 3))

    # # old
    # out[0, 0] = A
    # out[1, 1] = C
    # out[2, 2] = G
    #
    # out[1, 0] = out[0, 1] = B / 2
    #
    # out[2, 0] = out[0, 2] = D / 2
    #
    # out[2, 1] = out[1, 2] = F / 2

    # rectified
    out[0, 0] = G
    out[1, 1] = A
    out[2, 2] = C

    out[1, 0] = out[0, 1] = D / 2

    out[2, 0] = out[0, 2] = F / 2

    out[2, 1] = out[1, 2] = B / 2

    return out


def create_h5_dataset(images, A_capitulas):
    ids = np.linspace(0, len(images) - 1, num=len(images), dtype=int)
    random.shuffle(ids)
    print(int(3 * len(ids) / 5))
    sorted_ids = {"training": ids[: int(8 * len(ids) / 10)],
                  "validation": ids[int(8 * len(ids) / 10): int(9 * len(ids) / 10)],
                  "test": ids[int(9 * len(ids) / 10):]}

    # h5_file = '..\\mess\\dataset_agro_pepi_512.h5'
    h5_file = 'C:\\Users\\rgrousset\\PycharmProjects\\ellipse-rcnn-main\\ellipse-rcnn-main\\data\\simulated_ellipses_rectified_matrices.h5'
    f = h5py.File(h5_file, 'w')
    f.create_group("training")
    f.create_group("test")
    f.create_group("validation")
    for i in f.keys():
        # order to respect the [n, 3, 512, 512] dimensions of the model
        x = np.array(images, ndmin=1)[sorted_ids[i]]
        set_images = []
        for j in x:
            red = j[:, :, 0]
            green = j[:, :, 1]
            blue = j[:, :, 2]
            set_images.append(np.array([red, green, blue]))
        set_images = np.array(set_images)

        f[i].create_dataset("images", data=set_images)

        capitulas = np.array(A_capitulas, ndmin=1)[sorted_ids[i]]
        flatten_capitulas = []
        capitulas_idx = [0]
        a = 0
        for j in capitulas:
            a += 1
            print(a)
            flatten_capitulas = flatten_capitulas + j.tolist()
            capitulas_idx.append(capitulas_idx[-1] + len(j))

        labels = f[i].create_group("craters")
        labels.create_dataset("crater_list_idx", data=np.array(capitulas_idx))
        labels.create_dataset("A_craters", data=np.array(flatten_capitulas))


# PARAMETERS
patch_dims = (512, 512, 3)
nb_images = 2000


images = []
A_ellipses = []
for i in range(nb_images):
    ellipses = []
    image = np.zeros(patch_dims).astype(np.float32)
    image2 = cv2.imread("./data/images/patch1.png")
    nb_ellipses = random.randint(1, 2)
    nb_ellipses = 1
    for j in range(nb_ellipses):
        angle = random.random() * 180
        x = int(255)
        y = int(255)
        semi_major_axis = random.randint(20, 80)
        semi_major_axis = int(80)
        semi_minor_axis = random.randint(19, int(semi_major_axis))
        semi_minor_axis = int(40)

        cv2.ellipse(image, (int(x), int(y)), (int(semi_major_axis), int(semi_minor_axis)),
                    angle, 0, 360, (255, 0, 0), -1)
        cv2.ellipse(image, (int(x), int(y)), (int(semi_major_axis), int(semi_minor_axis)),
                    angle, 0, 360, (0, 0, 255), 3)
        #  0, endAngle=360, color=(0, 0, 255), thickness=4
        ellipses.append(conic_matrix(semi_major_axis, semi_minor_axis, - (angle * np.pi / 180) % np.pi, x=x, y=y).numpy())

        print("semi maj axis: " + str(semi_major_axis) + " || semi min axis: " + str(semi_minor_axis)
              + " || x: " + str(x) + " || y: " + str(y) + " || tau: " + str(angle))

    A_ellipses.append(ellipses)
    # plt.imshow(image)
    # plt.show()
    images.append(image)

create_h5_dataset(images, A_ellipses)






