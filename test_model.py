import matplotlib.pyplot as plt
import torch
import cv2
from ellipse_rcnn import get_dataloaders, EllipseRCNN
import numpy as np
from ellipse_rcnn.utils.conics import conic_center, ellipse_axes, ellipse_angle
from matplotlib.patches import Ellipse


def from_matrix_to_params_2(tensor):
    params = {"height": ellipse_axes(tensor)[0],
              "width": ellipse_axes(tensor)[1],
              "x": conic_center(tensor)[0],
              "y": conic_center(tensor)[1],
              "angle": ellipse_angle(tensor)}

    # mat = tensor.detach()
    # M0 = mat.numpy()
    #
    # # M = mat[1:, 1:].numpy()
    # #
    # # A = M0[1, 1]
    # # B = M0[2, 1] * 2
    # # C = M0[2, 2]
    # # D = M0[0, 1] * 2
    # # E = M0[0, 2]
    #
    # # # old
    # # a = np.linalg.inv(M0[:2, :2])
    # # b = np.expand_dims(-M0[:2, 2], axis=-1)
    # # c = a @ b
    # #
    # # lambdas = np.linalg.eigvalsh(M0[..., :2, :2]) / (-np.linalg.det(M0) / np.linalg.det(M0[..., :2, :2]))[..., None]
    #
    # # new
    # a = np.linalg.inv(M0[1:, 1:])
    # b = np.expand_dims(-M0[0, 1:], axis=1)
    # c = a @ b
    #
    # lambdas = np.linalg.eigvalsh(M0[..., 1:, 1:]) / (-np.linalg.det(M0) / np.linalg.det(M0[..., 1:, 1:]))[..., None]
    #
    # if lambdas[0] <= 0 or lambdas[1] <= 0:
    #     print("non acceptable values for lambda")
    #     axes = np.array([2000, 2000])
    # else:
    #     axes = np.sqrt(1 / lambdas)
    #
    # # eigenvalues = np.linalg.eig(M)
    # # determinant = np.linalg.det(M)
    # # params["height"] = np.sqrt(-np.linalg.det(M0) / (np.linalg.det(M) * eigenvalues[0][0]))
    # # params["width"] = np.sqrt(-np.linalg.det(M0) / (np.linalg.det(M) * eigenvalues[0][1]))
    # # params["x"] = (B * E - 2 * C * D) / (4 * A * C - B ** 2)
    # # params["y"] = (B * D - 2 * A * E) / (4 * A * C - B ** 2)
    # # params["angle"] = 0.5 * (np.pi / 2 - np.arctan((A - C) / D))
    #
    # params["height"] = axes[0]
    # params["width"] = axes[1]
    # if axes[0] >= axes[1]:
    #     params["height"] = axes[0]
    #     params["width"] = axes[1]
    # else:
    #     params["height"] = axes[1]
    #     params["width"] = axes[0]
    #     print("inversion d'axes")
    #
    # params["x"] = c[0][0]
    # params["y"] = c[1][0]
    #
    # # # old
    # # params["angle"] = np.arctan2(2 * M0[..., 1, 0], (M0[..., 0, 0] - M0[..., 1, 1])) / 2
    # # new
    # z = (M0[1, 1] - M0[2, 2]) / (2 * M0[1, 2])
    # params["angle"] = np.arctan(1 / z) / 2

    return params


def from_matrix_to_params(tensor):

        params = {"height": None,
                  "width": None,
                  "x": None,
                  "y": None,
                  "angle": None}

        mat = tensor.detach()
        M0 = mat.numpy()

        # M = mat[1:, 1:].numpy()
        #
        # A = M0[1, 1]
        # B = M0[2, 1] * 2
        # C = M0[2, 2]
        # D = M0[0, 1] * 2
        # E = M0[0, 2]

        a = np.linalg.inv(M0[:2, :2])
        b = np.expand_dims(-M0[:2, 2], axis=-1)
        c = a @ b

        lambdas = np.linalg.eigvalsh(M0[..., :2, :2]) / (-np.linalg.det(M0) / np.linalg.det(M0[..., :2, :2]))[..., None]

        if lambdas[0] <= 0 or lambdas[1] <= 0:
            print("non acceptable values for lambda")
            axes = np.array([2000, 2000])
        else:
            axes = np.sqrt(1 / lambdas)

        # eigenvalues = np.linalg.eig(M)
        # determinant = np.linalg.det(M)
        # params["height"] = np.sqrt(-np.linalg.det(M0) / (np.linalg.det(M) * eigenvalues[0][0]))
        # params["width"] = np.sqrt(-np.linalg.det(M0) / (np.linalg.det(M) * eigenvalues[0][1]))
        # params["x"] = (B * E - 2 * C * D) / (4 * A * C - B ** 2)
        # params["y"] = (B * D - 2 * A * E) / (4 * A * C - B ** 2)
        # params["angle"] = 0.5 * (np.pi / 2 - np.arctan((A - C) / D))

        params["height"] = axes[0]
        params["width"] = axes[1]
        if axes[0] >= axes[1]:
            params["height"] = axes[0]
            params["width"] = axes[1]
        else:
            params["height"] = axes[1]
            params["width"] = axes[0]
            print("inversion d'axes")

        params["x"] = c[0][0]
        params["y"] = c[1][0]
        params["angle"] = np.arctan2(2 * M0[..., 1, 0], (M0[..., 0, 0] - M0[..., 1, 1])) / 2

        return params


def plot_ellipses(ellipses, img):
    # plt.imshow(img[0])
    # plt.show()
    # plt.figure()

    # ax = plt.gca()

    np_img = img[0].detach().numpy().astype(np.uint32)
    swapped_img = np.empty((np_img.shape[1], np_img.shape[2], np_img.shape[0]), dtype=np.uint32)
    for i in range(np_img.shape[0]):
        swapped_img[:, :, i] = np_img[i, :, :]
    swapped_img = swapped_img.astype(np.float32)

    # ax.imshow(swapped_img)

    if ellipses[0]["ellipse_matrices"].ndim == 3:
        for i in ellipses[0]["ellipse_matrices"]:
            parameters = from_matrix_to_params(i)

            # ellipse = Ellipse(xy=(parameters["x"], parameters["y"]), width=parameters["width"], height=parameters["height"],
            #                   edgecolor='g', fc='None', lw=2, angle=((parameters["angle"] + np.pi / 2) / np.pi) * 180)
            # ax.add_patch(ellipse)
            cv2.ellipse(swapped_img, (int(parameters["x"]), int(parameters["y"])),
                        (int(parameters["height"]), int(parameters["width"])),
                        - 180 * parameters["angle"] / np.pi, 0, 360, (0, 255, 0), 3)

    else:
        # # old
        # parameters = from_matrix_to_params(ellipses[0]["ellipse_matrices"])

        # new
        parameters = from_matrix_to_params_2(ellipses[0]["ellipse_matrices"])

        # ellipse = Ellipse(xy=(parameters["x"], parameters["y"]), width=parameters["height"], height=parameters["width"],
        #                   edgecolor='g', fc='None', lw=2, angle=((parameters["angle"]) / np.pi) * 180)
        # ax.add_patch(ellipse)

        cv2.ellipse(swapped_img, (int(parameters["x"]), int(parameters["y"])),
                    (int(parameters["height"]), int(parameters["width"])),
                    float(180 * parameters["angle"] / np.pi), 0, 360, (0, 255, 0), 3)

    plt.imshow(swapped_img)

    plt.show()


# img = cv2.imread(".\\data\\images\\patch1.png")
# red = img[:, :, 2]
# green = img[:, :, 1]
# blue = img[:, :, 0]
# image = torch.from_numpy(np.array([[red, green, blue]]).astype(np.float32))

# ELLLLLLLIIIIIIPSSSSSEEEEEEEEEE borders
# loaded = torch.load("./data/results/saves/lightning_logs/version_78/checkpoints/epoch=174-step=8749.ckpt")
# loaded = torch.load("./data/results/saves/theta_pi/epoch=65-step=3299.ckpt")
loaded = torch.load("./data/results/saves/lightning_logs/version_90/epoch=27-step=1399.ckpt")
# loaded = torch.load(".\\data\\results\\saves\\lightning_logs\\version_75\\checkpoints\\epoch=129-step=6499.ckpt")

# # ELLLLLLLIIIIIIPSSSSSEEEEEEEEEE
# loaded = torch.load(".\\data\\results\\saves\\lightning_logs\\version_67\\checkpoints\\epoch=65-step=3299.ckpt")


# loaded = torch.load(".\\data\\results\\saves\\epoch=69-step=3709.ckpt")
# loaded = torch.load(".\\data\\results\\saves\\lightning_logs\\version_62\\checkpoints\\epoch=91-step=4875.ckpt")
# loaded = torch.load(".\\data\\results\\saves\\lightning_logs\\version_53\\checkpoints\\epoch=4-step=249.ckpt")
# loaded = torch.load(".\\data\\results\\saves\\lightning_logs\\version_52\\checkpoints\\epoch=117-step=6253.ckpt")
# loaded = torch.load(".\\data\\results\\saves\\lightning_logs\\version_39\\checkpoints\\epoch=112-step=10056.ckpt")
# loaded = torch.load(".\\data\\results\\saves\\lightning_logs\\version_41\\checkpoints\\epoch=67-step=3603.ckpt")
# loaded = torch.load(".\\data\\results\\saves\\lightning_logs\\version_43\\checkpoints\\epoch=43-step=2331.ckpt")
# loaded = torch.load(".\\data\\results\\saves\\lightning_logs\\version_49\\checkpoints\\epoch=3-step=211.ckpt")


# loaded = torch.load(".\\lightning_logs\\version_26\\checkpoints\\epoch=149-step=8249.ckpt")

# # ###########################################################################################################################################
# image = np.load(".\\data\\test_image.npy")
# loaded = torch.load(".\\data\\CraterRCNN.pth")
#
# model = EllipseRCNN()
# model.load_state_dict(loaded)
# model.eval()
#
# img = torch.from_numpy(image)
# img = img.view(1, 256, 256)
# out = model([img])
#
# plot_ellipses(out, img)
# # #########################################################################################################################################

# model.eval()
model = EllipseRCNN(min_size=512, rpn_batch_size_per_image=512)
model.load_state_dict(loaded["state_dict"])
model.eval()

train_loader, validation_loader, test_loader = get_dataloaders(
        ".\\data\\simulated_ellipses_rectified_matrices.h5", batch_size=1)

out = []

for i in range(100):
        image, targets = next(iter(test_loader))
        result = model(image)
        plot_ellipses(result, image)
        out.append(result)

print("stop")


# img = cv2.imread(".\\data\\images\\patch1.png")
# checkpoint = ".\\data\\results\\saves\\lightning_logs\\version_0\\checkpoints\\epoch=99-step=5499.ckpt"
# model = CraterDetector()
# model.load_state_dict(torch.load(checkpoint))
# model.to('cuda')
# out = model(img)
#
# print("stop")
